Task: make delay for sending BYE to carrier after received from user side

1. if user send BYE and duration < 9, then add pause = (9 - duration).
2. if user send BYE after 9 seconds, then no delay
3. if carrier sends BYE first then re-send to user without delay
4. if carrier sends BYE in pause period (user sended BYE first) then opensips will answer 200 ok from internal feature.